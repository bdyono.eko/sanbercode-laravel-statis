<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('register');
    }

    public function welcome(Request $request){
        $data = [
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'gender' => $request['gender'],
            'nationality' => $request['nationality'],
            'language1' => $request['language1'],
            'language2' => $request['language2'],
            'language3' => $request['language3'],
            'bio' => $request['bio'],
        ];

        return view('welcome', compact('data'));
    }
}
