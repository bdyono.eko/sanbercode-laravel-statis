@extends('templates.master')
@section('judul')
  Selamat Datang
@endSection
@section('judul2')
  Welcome {{$data['first_name']}} {{$data['last_name']}}
@endSection
@section('content')
  <h3>Profile</h3>
  <p><b>Name:</b> {{$data['first_name']}} {{$data['last_name']}} </p>
  <p><b>Gender:</b> {{$data['gender']}} </p>
  <p><b>Nationality:</b> {{$data['nationality']}} </p>
  <p><b>Language Speakers:</b> {{$data['language1']}}, {{$data['language2']}}, {{$data['language3']}} </p>
  <p><b>Bio:</b> {{$data['bio']}} </p>
@endSection
