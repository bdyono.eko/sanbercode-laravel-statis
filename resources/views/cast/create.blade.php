@extends('templates.master')

@section('judul')
  Tambah Cast
@endSection

@section('judul2')
  Form Tambah Cast
@endSection

@section('content')
	<div class="col-md-8">
		<form class="form-horizontal" action="/cast" method="post">
			@csrf
			<div class="card-body">
				<div class="form-group row">
					<label for="nama" class="col-sm-2 col-form-label">Nama</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama...">
						@error('nama')
							<div class="alert alert-danger mt-1">
								{{ $message }}
							</div>
						@enderror
					</div>
				</div>
				<div class="form-group row">
					<label for="umur" class="col-sm-2 col-form-label">Umur</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="umur" name="umur" placeholder="Umur...">
						@error('umur')
							<div class="alert alert-danger mt-1">
								{{ $message }}
							</div>
						@enderror
					</div>
				</div>
				<div class="form-group row">
					<label for="bio" class="col-sm-2 col-form-label">Bio</label>
					<div class="col-sm-10">
						<textarea class="form-control" rows="3" placeholder="Bio..." name="bio" id="bio"></textarea>
						@error('bio')
							<div class="alert alert-danger mt-1">
								{{ $message }}
							</div>
						@enderror
					</div>
				</div>
				<div class="row justify-content-end">
					<button type="submit" class="btn btn-info">Tambah</button>
				</div>
			</div>
		</form>
	</div>
@endSection