@extends('templates.master')

@section('judul')
  Cast
@endSection

@section('judul2')
  Daftar Cast
@endSection

@section('content')
  <a href="/cast/create" class="btn btn-primary">Tambah Cast</a>
  <table class="table mt-3">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $a)
        <tr>
          <td>{{ $key + 1  }}</td>
          <td>{{ $a->nama }}</td>
          <td>{{ $a->umur }}</td>
          <td>{{ $a->bio }}</td>
          <td>
            <a href="/cast/{{ $a->id }}" class="btn btn-info my-1">Show</a>
            <a href="/cast/{{ $a->id }}/edit" class="btn btn-primary my-1">Edit</a>
            <div class="form-check form-check-inline">
              <form action="/cast/{{ $a->id }}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger my-1">
              </form>
            </div>
          </td>
        </tr>
      @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>  
      @endforelse
    </tbody>
  </table>
@endSection