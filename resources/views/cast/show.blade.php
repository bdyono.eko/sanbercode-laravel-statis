@extends('templates.master')

@section('judul')
  Detail
@endSection

@section('judul2')
  Detail Cast
@endSection

@section('content')
  <div class="col-md-8">
    <div class="card-body">
      <div class="row form-group">
        <label class="col-sm-2 col-form-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{{ $cast->nama }}" readonly />
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 col-form-label">Umur</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{{ $cast->umur }}" readonly />
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 col-form-label">Bio</label>
        <div class="col-sm-10">
          <textarea class="form-control" cols="30" rows="3" readonly> {{ $cast->bio }} </textarea>
        </div>
      </div>
    </div>
  </div>
@endSection