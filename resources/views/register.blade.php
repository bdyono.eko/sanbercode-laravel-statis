@extends('templates.master')
@section('judul')
  Buat Akun Baru
@endSection
@section('judul2')
  Sign Up Form
@endSection
@section('content')
<form action="welcome" method="post">
  @csrf
  <label for="first_name">First Name</label><br>
  <input type="text" name="first_name" id="first_name">
  <br><br>

  <label for="last_name">Last Name</label><br>
  <input type="text" name="last_name" id="last_name">
  <br><br>

  <label for="gender">Gender</label><br>
  <input type="radio" name="gender" id="gender" value="Man"> Man <br>
  <input type="radio" name="gender" id="gender" value="Woman"> Woman <br>
  <input type="radio" name="gender" id="gender" value="Other"> Other 
  <br><br>

  <label for="nationality">Nationality</label><br>
  <select name="nationality" id="nationality">
    <option value="Indonesia">Indonesia</option>
    <option value="Jepang">Jepang</option>
    <option value="German">German</option>
  </select>
  <br><br>

  <label for="language">Language Speakers</label><br>
  <input type="checkbox" name="language1" id="language" value="Bahasa Indonesia"> Bahasa Indonesia <br>
  <input type="checkbox" name="language2" id="language" value="Japanese"> Japanese <br>
  <input type="checkbox" name="language3" id="language" value="English"> English 
  <br><br>

  <label for="bio">Bio</label><br>
  <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
  <br><br>

  <button type="submit">Sign Up</button>
</form>
@endSection

